/**
 * this js file helps us in sending notifications
 * Created by Sai Umesh on 12/22/2016.
 */

var gcm = require('node-gcm');


exports.sendNotification = function(req,res){
    var message = new gcm.Message({
        data: { key1: 'msg1' },
        notification:{
            title: 'Title of your push notification',
            body: 'Body of your push notification'
        }
    });
    // please change this send key with your key
    var sender = new gcm.Sender('AIzaSyDQd-FH8ovYKDKHEYTh5CZc03-FWVPZR-4');
    var regTokens = [req.body.token];
    sender.send(message, { registrationTokens: regTokens }, function (err, response) {
        if (err) {
            res.send("error occurred check console for more information");
            console.log("firebase error ",err);
        } else {
            res.send("successfully sent firebase message");
        }
    });
};


// now you can send topics as well using below code
var FCM = require('fcm-push');
exports.sendTopics = function(req,res){
    var serverKey = 'AAAALPxyMzo:APA91bES1BBAIbFQKZ4HF0qM30Z0xBIQM7Ns1Yi10LbtoqYmHZQGcJnoeVdnqv-kcp9jIj2d4lr-HF5yeWRyRJv7QT-QiJFuwgJM-SbBeWmExGOF7V0MylmEk7GBlNyWtbGXH-Sa3EKX9xIwBuSdMcIiZaOBW0ylhg';
    var fcm = new FCM(serverKey);
    var message = {
        to: '/topics/Accenture', // required fill with device token or topics
        data: {
            your_custom_data_key: 'your_custom_data_value'
        },
        notification: {
            title: 'Title of your push notification',
            body: 'Body of your push notification'
        }
    };

    //callback style
    fcm.send(message, function(err, response){
        if (err) {
            console.error("error ",err);
            console.log("Something has gone wrong!");
            res.send("error");
        } else {
            res.send("successfully sent firebase message");
            console.log("Successfully sent with response: ", response);
        }
    });
};